from aiohttp import ClientSession
from asyncache import cached
from cachetools import TTLCache, keys
from datetime import datetime, date, timedelta
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class HoaHandler(RequestHandler):
    template: Template
    hoa_date: datetime.date

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template("hoa.htm.jinja")
        self.session = session

    @cached(cache=TTLCache(maxsize=1, ttl=600), key=keys.methodkey)
    async def get_json(self) -> dict:
        url = "https://hackenopenair.de/infos.json"
        async with self.session.get(url) as response:
            return await response.json()

    async def get_days_until_hoa(self) -> timedelta:
        today = date.today()

        info = await self.get_json()
        start = info["next"]["start"]
        future = datetime.strptime(start, "%Y-%m-%d")
        diff = future.date() - today

        return diff.days

    async def get(self):
        """
        Get days until HOA starts
        """

        days = await self.get_days_until_hoa()

        self.write(self.template.render(days=days))
