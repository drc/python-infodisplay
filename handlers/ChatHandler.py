import json
from os import environ
from urllib.parse import quote as encode

from aiohttp import ClientSession
from arrow import Arrow
from asyncache import cached
from cachetools import TTLCache, keys
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class ChatHandler(RequestHandler):
    """
    ```
    import requests
    payload = {"password": password, "identifier": {"type": "m.id.user", "user": "infodisplay"},
           "initial_device_display_name": "chathandler", "type": "m.login.password"}

    response = requests.post("https://matrix.stratum0.org/_matrix/client/v3/login", json=payload).json()
    ```

    to retrieve new token in case it goes missing
    """

    template: Template
    headers: dict
    room_id: str
    session: ClientSession

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template('chat.htm.jinja')
        self.headers = {
            "Authorization": f"Bearer {environ.get('MATRIX_TOKEN')}",
            "Content-Type": "application/json"
        }
        self.room_id = "!rusSJndyAbjDmlzXIc:stratum0.org"  # Internal ID for #stratum0:stratum0.org
        self.session = session

    @cached(cache=TTLCache(maxsize=1, ttl=60), key=keys.methodkey)
    async def get_messages(self, count: int = 20) -> list:
        message_filter = json.dumps({"types": ["m.room.message"]})
        parameters = f"dir=b&limit={count}&filter={encode(message_filter)}"
        url = f"https://matrix.stratum0.org/_matrix/client/v3/rooms/{self.room_id}/messages?{parameters}"
        messages = []

        async with self.session.get(url, headers=self.headers) as response:
            matrix_json = await response.json()
            for event in matrix_json.get("chunk", []):
                if event.get("type") == "m.room.message":
                    try:
                        messages.append({
                            "time": Arrow.utcfromtimestamp(event["origin_server_ts"]).to("local"),
                            "timestamp": event["origin_server_ts"],
                            "sender": event["sender"][1:].rpartition(':')[0],  # Matrix usernames are weird
                            "content": event["content"]["body"]
                        })
                    except KeyError:
                        continue

            messages.sort(key=lambda x: x["timestamp"])
            return messages

    async def get(self):
        """
        Get last messages from matrix
        """
        messages = await self.get_messages()

        self.write(self.template.render(messages=messages))
