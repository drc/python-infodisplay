from os import environ

import arrow
import plotly.graph_objects as go
from aiohttp import ClientSession
from asyncache import cached
from cachetools import TTLCache, keys
from jinja2 import Template, Environment
from plotly.subplots import make_subplots
from tornado.web import RequestHandler


class DiagramHandler(RequestHandler):
    template: Template
    session: ClientSession
    headers: dict

    def initialize(self, jinja: Environment, session: ClientSession):
        self.template = jinja.get_template('diagram.htm.jinja')
        self.headers = {"Authorization": f"Bearer {environ.get('HOMEASSISTANT_TOKEN')}"}
        self.session = session

    @cached(cache=TTLCache(maxsize=2, ttl=55), key=keys.methodkey)
    async def get_sensor_data(self, sensor: str) -> (list, list):
        parameters = f"no_attributes&significant_changes_only&filter_entity_id=sensor.{sensor}"
        url = f"http://homeassistant.s0:8123/api/history/period?{parameters}"

        async with self.session.get(url, headers=self.headers) as response:
            json = await response.json()

        x = []
        y = []
        for entity in json[0]:
            try:
                value = int(entity.get("state"))
                date = arrow.get(entity.get("last_changed"))
            except ValueError:
                continue

            x.append(date)
            y.append(value)

        return x, y

    @cached(cache=TTLCache(maxsize=1, ttl=55), key=keys.methodkey)
    async def make_html(self) -> str:
        power_x, power_y = await self.get_sensor_data("powespr0_power_meter_space")
        clients_x, clients_y = await self.get_sensor_data("stratum0")

        fig = make_subplots(specs=[[{"secondary_y": True}]])
        # Add traces
        fig.add_trace(
            go.Scatter(x=power_x, y=power_y, name="Power consumption"),
            secondary_y=False,
        )

        fig.add_trace(
            go.Scatter(x=clients_x, y=clients_y, name="WiFi client count", line_shape="hv"),
            secondary_y=True,
        )
        fig.update_layout(
            #showlegend=False,
            legend=dict(
                yanchor="top",
                y=0.99,
                xanchor="right",
                x=0.93,
                bgcolor="#073642"
            ),
            paper_bgcolor='rgba(0,0,0,0)',
            font_color='#839496',
            plot_bgcolor='#002b36',
            margin=dict(l=0, r=0, t=50, b=10),
        )

        fig.update_xaxes(showgrid=True, gridwidth=1, gridcolor='#073642',)
        fig.update_yaxes(showgrid=True, gridwidth=1, gridcolor='#073642', range=[0, None], secondary_y=True)
        fig.update_yaxes(showgrid=False, zeroline=True, zerolinewidth=1, zerolinecolor='#073642', range=[0, None], secondary_y=False)

        # Set y-axes titles
        fig.update_yaxes(title_text="<b>Power</b> consumption (W)", secondary_y=False)
        fig.update_yaxes(title_text="<b>WiFi clients</b>", secondary_y=True)

        return fig.to_html(full_html=False, config=dict(staticPlot=True))

    async def get(self):
        html = await self.make_html()

        self.write(self.template.render(chart=html))
