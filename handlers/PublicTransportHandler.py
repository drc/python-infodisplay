import json
from xml.etree import ElementTree

import arrow
from aiohttp import ClientSession, ClientConnectorError
from asyncache import cached
from cachetools import TTLCache, keys
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class PublicTransportHandler(RequestHandler):
    template: Template
    stops: list[str] = ['Ludwigstraße', 'Hamburger Straße']
    city: str = 'Braunschweig'
    team: str = 'Eintracht Braunschweig'
    session: ClientSession

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template('publictransport.htm.jinja')
        self.session = session

    @cached(cache=TTLCache(maxsize=2, ttl=60 * 60 * 24), key=keys.methodkey)
    async def get_match_info(self) -> dict:
        """
        Use data from https://github.com/openfootball/football.json/ to determine all matches
        for the configured team in the current season
        """
        today = arrow.now("local")
        if today.month > 6:
            next_year = today.shift(years=1)
            season = f"{today.year}-{next_year.strftime('%y')}"
        else:
            last_year = today.shift(years=-1)
            season = f"{last_year.year}-{today.strftime('%y')}"

        url = f"https://raw.githubusercontent.com/openfootball/football.json/master/{season}/de.2.json"

        async with self.session.get(url) as response:
            payload = await response.read()
            return json.loads(payload)

    async def parse_match_info(self, dates: list):
        schedule = await self.get_match_info()
        return [m for m in schedule['matches'] if m['team1'] == self.team and m['date'] in dates]

    @cached(cache=TTLCache(maxsize=2, ttl=60 * 60), key=keys.methodkey)
    async def get_stop_info(self, stop: str) -> bytes:
        url = f"https://bsvg.efa.de/bsvagstd/XML_DM_REQUEST?sessionID=0&requestID=0&language=de&useRealtime=1&coordOutputFormat=WGS84[DD.ddddd]&locationServerActive=1&mode=direct&dmLineSelectionAll=1&depType=STOPEVENTS&useAllStops=1&command=null&type_dm=stop&name_dm={self.city}%20{stop}&mId=efa_rc2"

        async with self.session.get(url) as response:
            return await response.read()

    async def parse_stop_info(self, stop: str, count: int = 6) -> list:
        byte_response = await self.get_stop_info(stop)
        tree = ElementTree.fromstring(byte_response)
        departures = tree.find('itdDepartureMonitorRequest').find('itdDepartureList')

        info = []
        now = arrow.now('local')
        for departure in list(departures):
            # Extract departure time from XML
            date_time = departure.find('itdDateTime')
            date = date_time.find('itdDate')
            time = date_time.find('itdTime')
            departure_time = arrow.Arrow(
                year=int(date.get('year')),
                month=int(date.get('month')),
                day=int(date.get('day')),
                hour=int(time.get('hour')),
                minute=int(time.get('minute')),
                tzinfo='local'
            )

            if departure_time < now:
                continue

            line = departure.find('itdServingLine')
            serving_line = line.get('symbol')

            info.append({
                'line': serving_line,
                # This is just how the BSVG names things
                'type': 'tram' if len(serving_line) < 3 else 'bus',
                'dir': line.get('direction').replace(self.city, '').strip(),
                'platform': departure.get('platform'),
                'time': arrow.Arrow(
                    year=int(date.get('year')),
                    month=int(date.get('month')),
                    day=int(date.get('day')),
                    hour=int(time.get('hour')),
                    minute=int(time.get('minute')),
                    tz="local"
                )
            })

            if len(info) >= count:
                break

        return info

    async def get(self, count: int = 6):
        """
        Get the next departures at the defined stops, check for upcoming home matches
        """
        today = arrow.now('local')
        tomorrow = today.shift(days=1)
        matches = await self.parse_match_info(
            [str(today.date()), str(tomorrow.date())]
        )

        stops = {}
        for stop in self.stops:
            try:
                stops[stop] = await self.parse_stop_info(stop, count)
            except ClientConnectorError as e:
                stops[stop] = e.strerror

        self.write(self.template.render(stops=stops, now=today, matches=matches))
