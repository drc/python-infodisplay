from itertools import islice

from datetime import datetime
from aiohttp import ClientSession
from asyncache import cached
from cachetools import TTLCache, keys
from icalendar import Calendar
import recurring_ical_events
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class CalendarHandler(RequestHandler):
    template: Template
    session: ClientSession

    def initialize(self, jinja: Environment, session: ClientSession) -> None:
        self.template = jinja.get_template("events.htm.jinja")
        self.session = session

    @cached(cache=TTLCache(maxsize=1, ttl=60 * 60 * 4), key=keys.methodkey)
    async def parse_ical(self) -> recurring_ical_events.CalendarQuery:
        url = "https://stratum0.org/kalender/termine.ics"

        async with self.session.get(url) as response:
            calendar = Calendar.from_ical(await response.read())
            return recurring_ical_events.of(calendar)

    async def get(self, count: int = 10):
        """
        Get the next calendar events
        """
        events = await self.parse_ical()
        upcoming = islice(events.after(datetime.now()), count)

        self.write(self.template.render(events=upcoming))
