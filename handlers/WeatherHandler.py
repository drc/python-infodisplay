from datetime import timedelta
from typing import Optional

import arrow
from aiohttp import ClientSession
from asyncache import cached
from cachetools import TTLCache, keys
from jinja2 import Template, Environment
from tornado.web import RequestHandler


class WeatherHandler(RequestHandler):
    template: Template
    # DWD station ID, list on
    # https://www.dwd.de/DE/leistungen/klimadatendeutschland/statliste/statlex_html.html?view=nasPublication&nn=16102
    station_id: str
    session: ClientSession
    icon_map: dict[int, tuple] = {
        1: ('☼', '#b58900'),  # Sonne
        2: ('☼', '#b58900'),  # Sonne, leicht bewölkt
        3: ('☼', '#b58900'),  # Sonne, bewölkt
        4: ('☁', 'gray'),  # Wolken
        5: ('🌫', 'gray'),  # Nebel
        6: ('🌫', 'gray'),  # Nebel, rutschgefahr
        7: ('⛆', '#268bd2'),  # leichter Regen
        8: ('⛆', '#268bd2'),  # Regen
        9: ('⛆', '#268bd2'),  # starker Regen
        10: ('⛆', '#268bd2'),  # leichter Regen, rutschgefahr
        11: ('⛆', '#268bd2'),  # starker Regen, rutschgefahr
        12: ('⛆', '#268bd2'),  # Regen, vereinzelt Schneefall
        13: ('⛆', '#268bd2'),  # Regen, vermehrt Schneefall
        14: ('❄', 'white'),  # leichter Schneefall
        15: ('❄', 'white'),  # Schneefall
        16: ('❄', 'white'),  # starker Schneefall
        17: ('⛆', 'white'),  # Wolken, (Hagel)
        18: ('☼', '#b58900'),  # Sonne, leichter Regen
        19: ('⛆', '#268bd2'),  # Sonne, starker Regen
        20: ('⛆', '#268bd2'),  # Sonne, Regen, vereinzelter Schneefall
        21: ('❄', 'white'),  # Sonne, Regen, vermehrter Schneefall
        22: ('❄', 'white'),  # Sonne, vereinzelter Schneefall
        23: ('❄', 'white'),  # Sonne, vermehrter Schneefall
        24: ('⛆', 'white'),  # Sonne, (Hagel)
        25: ('⛆', 'white'),  # Sonne, (staker Hagel)
        26: ('🗲', '#b58900'),  # Gewitter
        27: ('🗲', '#b58900'),  # Gewitter, Regen
        28: ('🗲', '#b58900'),  # Gewitter, starker Regen
        29: ('🗲', '#b58900'),  # Gewitter, (Hagel)
        30: ('🗲', '#b58900'),  # Gewitter, (starker Hagel)
        31: ('☴', 'white'),  # (Wind)
    }

    def initialize(self, jinja: Environment, station_id: str, session: ClientSession) -> None:
        self.template = jinja.get_template('weather.htm.jinja')
        self.station_id = station_id
        self.session = session

    @cached(cache=TTLCache(maxsize=1, ttl=60 * 60 * 4), key=keys.methodkey)
    async def get_weather(self):
        url = f"https://app-prod-ws.warnwetter.de/v30/stationOverviewExtended?stationIds={self.station_id}"

        async with self.session.get(url) as response:
            return await response.json()

    async def get(self):
        json = await self.get_weather()

        # Pre-process forecast
        days = []
        for day in json.get(self.station_id, {}).get("days")[1:]:
            if len(days) > 5:
                break

            days.append({
                'date': arrow.get(day.get('dayDate')).to('local'),
                'icon': self.icon_map[day.get('icon')],
                'temperatureMin': day.get('temperatureMin') / 10,
                'temperatureMax': day.get('temperatureMax') / 10
            })

        forecast = json.get(self.station_id, {}).get("forecast1")

        time: Optional[arrow.Arrow] = None
        temperature = None
        icon = ('?', 'white')
        if forecast:
            # DWD forecasts always start at the beginning of the day
            now = arrow.utcnow()
            start = arrow.Arrow.utcfromtimestamp(forecast.get("start") / 1000)
            diff = now - start

            # Calculate offset to current time
            step = int(forecast.get("timestep", 3600000) / 1000)
            timeslot = int(diff.total_seconds() / step)

            # Get forecasted temperature for "now"
            time = start + timedelta(seconds=timeslot * step)
            temperatures = forecast.get("temperature", [])
            icons = forecast.get("icon", [])
            temperature = temperatures[timeslot] if timeslot < len(temperatures) else None
            icon = self.icon_map[icons[timeslot]] if timeslot < len(icons) else ('?', 'white')

        self.write(self.template.render(
            days=days,
            time=time.to('local'),
            temperature=temperature,
            icon=icon
        ))
